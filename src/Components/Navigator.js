import React from 'react';
import * as ReactBootStrap from 'react-bootstrap';
import Cookies from 'js-cookie';
import Logo from './transparentLogo.png'





export default class Navigator extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      login: true,
      logout: false

    }
  }







  render() {


    return (
      <div className="Navigator">
        <ReactBootStrap.Navbar collapseOnSelect expand="lg"  >
          <ReactBootStrap.Navbar.Brand href="http://localhost:3000" className="airgo-logo" >

            <img src={Logo}
              width="51"
              height="51"
              alt=''
            />
          </ReactBootStrap.Navbar.Brand>
          <ReactBootStrap.Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <ReactBootStrap.Navbar.Collapse id="responsive-navbar-nav">
            <ReactBootStrap.Nav className="mr-auto">
              {Cookies.get('JSESSIONID') === undefined && <ReactBootStrap.Nav.Link href="http://localhost:8080/login" >Login</ReactBootStrap.Nav.Link>}
              {Cookies.get('JSESSIONID') && <ReactBootStrap.Nav.Link href="http://localhost:8080/logout" >Logout</ReactBootStrap.Nav.Link>}
              {Cookies.get('JSESSIONID') === undefined && <ReactBootStrap.Nav.Link href="http://localhost:3000/Register">Register</ReactBootStrap.Nav.Link>}
            </ReactBootStrap.Nav>

          </ReactBootStrap.Navbar.Collapse>
        </ReactBootStrap.Navbar>
      </div>
    )
  }
}
