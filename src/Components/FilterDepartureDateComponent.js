import React from 'react';
import "react-datepicker/dist/react-datepicker.css";

export default class FilterDeparatureDateComponent extends React.Component {

    constructor() {
        super()
        this.state = { searchedValue: '' }
    }

    onSearch = (event) => {
        this.props.setFilter(event.target.value)
    }



    render() {


        return (
            <div class="bootstrap-iso">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">


                            <form method="post">
                                <div class="form-group" data-component="search">

                                    <input class="form-control" placeholder="YYYY-MM-DD" type="text" onChange={this.onSearch} value={this.props.filter} />



                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>

        )
    }






}