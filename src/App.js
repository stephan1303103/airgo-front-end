import './App.css';
import Airgofrontpage from './Components/Airgofrontpage';
import RegisterComponent from './Components/RegisterComponent';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";


function App() {
  return (
    
    <Router>
       <Route exact path="/" component={Airgofrontpage} />
       <Route exact path="/Register" component={RegisterComponent}/>
      </Router>
  );
}

export default App;
